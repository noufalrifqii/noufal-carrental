# Binar Car Rental - Challenge 3 - Muhammad Noufal Rifqi Iman

Sebuah responsive website yang berdasarkan dari [desain](https://www.figma.com/file/QiNXZPX7OwUeFzqSPuiQBE/BCR---Binar-Car-Rental?node-id=2%3A703) berikut.

## Deskripsi

Project ini merupakan challenge ke-3 untuk program Fullstack Web Development di Binar Academy. Project ini juga merupakan sebuah responsive website untuk berbagai macam perangkat sepert mobile, tablet dan desktop. Untuk project ini, saya menggunakan html, css, boostrap 5 dan jquery serta Visual Studio Code sebagai text editor.

## Instalasi dan Penggunaan

Clone atau download repository berikut ke dalam local storage anda. Kemudian buka file index.html dengan menggunakan browser favorit anda. Anda dapat membukanya dalam berbagai dimensi dari mobile, tablet hingga desktop. Selain itu juga anda dapat mengubah file index.html tersebut sesuai kehendak anda.
